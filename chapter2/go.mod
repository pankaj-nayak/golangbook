module covid/jaknap/chapter2

go 1.14

require (
	github.com/dgraph-io/dgo v1.0.0
	github.com/dgraph-io/dgo/v2 v2.2.0
	github.com/go-delve/delve v1.4.0 // indirect
	github.com/golang/protobuf v1.3.3
	google.golang.org/grpc v1.29.0
)
